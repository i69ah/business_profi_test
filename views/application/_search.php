<?php

use app\models\ApplicationStatus;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="d-flex flex-row flex-wrap">
        <?= $form->field($model, 'status', [
            'inputOptions' => [
                'class' => 'form-control',
            ],
            'options' => [ 'class' => 'form-group mr-2' ],
        ])->dropDownList(ApplicationStatus::getAssocList()) ?>

        <?= $form->field($model, 'created_at_from', [
            'inputOptions' => [
                'type' => 'date',
                'class' => 'form-control'
            ],
            'options' => [ 'class' => 'form-group mr-2' ],
        ]) ?>
        <?= $form->field($model, 'created_at_to', [
            'inputOptions' => [
                'type' => 'date',
                'class' => 'form-control'
            ],
            'options' => [ 'class' => 'form-group mr-2' ],
        ]) ?>

        <?= $form->field($model, 'last_check_status_at_from', [
            'inputOptions' => [
                'type' => 'date',
                'class' => 'form-control'
            ],
            'options' => [ 'class' => 'form-group mr-2' ],
        ]) ?>
        <?= $form->field($model, 'last_check_status_at_to', [
            'inputOptions' => [
                'type' => 'date',
                'class' => 'form-control'
            ],
            'options' => [ 'class' => 'form-group mr-2' ],
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сбросить', ['class' => 'btn btn-default', 'id' => 'rstBtn']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>