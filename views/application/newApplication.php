<?php
/** @var ApplicationForm $model */
/** @var string $title */

use app\models\ApplicationForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJsFile(
    '@web/js/checkDouble.js',
    [ 'position' => $this::POS_END ],
);

$form = ActiveForm::begin([
    'errorCssClass' => 'text-danger',
    'successCssClass' => 'text-success',
    'validateOnSubmit' => true,
    'options' => [
        'id' => 'mainForm'
    ]
]);
?>
<h1><?= $title ?></h1>
<?= $form->field($model, 'organisationName') ?>
<?= $form->field($model, 'inn') ?>
<?= $form->field($model, 'ogrn') ?>
<?= $form->field($model, 'kpp') ?>
<?= $form->field($model, 'fullName') ?>
<?= $form->field($model, 'phoneNumber') ?>
<?= $form->field($model, 'contactEmail') ?>
<?= $form->field($model, 'contractorsPosition') ?>
<?= $form->field($model, 'extraPhoneNumbers') ?>
<?= $form->field($model, 'comment') ?>
<?= $form->field($model, 'cityCode') ?>
<?= $form->field($model, 'productCode') ?>
<?= $form->field($model, 'branchId') ?>
<?= $form->field($model, 'address') ?>
<?= $form->field($model, 'email') ?>
<div class="btn-toolbar">
    <div class="btn-group mr-2">
        <?= Html::button('Проверить на дубль', [
            'id' => 'checkDoubleBtn',
            'class' => 'btn btn-primary'
        ]) ?>
    </div>
    <div class="btn-group">
        <?= Html::submitButton('Отправить', [
            'id' => 'sendBtn',
            'class' => 'btn btn-primary',
            'disabled' => true,
        ]) ?>
    </div>
</div>
<div class="error-display mt-2">
    <div class="alert alert-danger d-none" id="alert-danger-template"></div>
</div>
<?php ActiveForm::end() ?>