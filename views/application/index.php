<?php

/** @var ActiveDataProvider $dataProvider */
/** @var Application $filterModel */

use app\components\City;
use app\models\Application;
use app\models\ApplicationForm;
use app\models\ApplicationStatus;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

$applicationForm = new ApplicationForm();
?>
<?= $this->render('_search', ['model' => $filterModel]) ?>
<div class="overflow-auto">
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'organisation_name',
            'label' => $applicationForm->getAttributeLabel('organisationName'),
        ],
        [
            'attribute' => 'inn',
            'label' => $applicationForm->getAttributeLabel('inn'),
        ],
        [
            'attribute' => 'ogrn',
            'label' => $applicationForm->getAttributeLabel('ogrn'),
        ],
        [
            'attribute' => 'kpp',
            'label' => $applicationForm->getAttributeLabel('kpp'),
        ],
        [
            'attribute' => 'full_name',
            'label' => $applicationForm->getAttributeLabel('fullName'),
        ],
        [
            'attribute' => 'phone_number',
            'label' => $applicationForm->getAttributeLabel('phoneNumber'),
        ],
        [
            'attribute' => 'contact_email',
            'label' => $applicationForm->getAttributeLabel('contactEmail'),
        ],
        [
            'attribute' => 'contractors_position',
            'label' => $applicationForm->getAttributeLabel('contractorsPosition'),
        ],
        [
            'attribute' => 'extra_phone_numbers	',
            'label' => $applicationForm->getAttributeLabel('extraPhoneNumbers'),
        ],
        [
            'attribute' => 'comment	',
            'label' => $applicationForm->getAttributeLabel('comment'),
        ],
        [
            'attribute' => 'city_code',
            'label' => 'Регион',
            'content' => function (Application $model) {
                return (new City($model->city_code))->getTitle();
            }
        ],
        [
            'attribute' => 'product_code',
            'label' => $applicationForm->getAttributeLabel('productCode'),
        ],
        [
            'attribute' => 'branch_id',
            'label' => $applicationForm->getAttributeLabel('branchId'),
        ],
        [
            'attribute' => 'address',
            'label' => $applicationForm->getAttributeLabel('address'),
        ],
        [
            'attribute' => 'email',
            'label' => $applicationForm->getAttributeLabel('email'),
        ],
        [
            'attribute' => 'created_at',
            'label' => 'Дата создания',
        ],
        [
            'attribute' => 'last_check_status_at',
            'label' => 'Дата последнего обновления статуса',
        ],
        [
            'attribute' => 'status',
            'label' => 'Статус заявки',
            'filter' => false,
            'content' => function (Application $model) {
                $status = new ApplicationStatus($model->status);
                $classSpan = match (true) {
                    $status->isInWork() => 'text-secondary',
                    $status->isDouble() => 'text-warning',
                    $status->isRejected() => 'text-danger',
                    $status->isSuccess() => 'text-success',
                };

                return Html::tag('span', $status->getTitle(), [
                    'class' => $classSpan,
                ]);
            }
        ],
        [
            'attribute' => 'alfa_id',
            'label' => 'Альфа ID',
        ],
    ],
]) ?>
</div>
