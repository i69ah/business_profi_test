document.querySelector('#checkDoubleBtn').addEventListener('click', function (e) {
    e.preventDefault()
    fetch('/web/index.php?r=application/ajax-check-double', {
        method: 'POST',
        body: new FormData(document.querySelector('#mainForm'))
    }).then(async (rawResponse) => {
        debugger
        if (200 !== rawResponse.status) {
            let uint8array = (await rawResponse.body.getReader().read()).value;
            let string = JSON.parse(new TextDecoder().decode(uint8array)).message;
            throw new Error(rawResponse.statusText + " " + string)
        }

        return rawResponse.json()
    }).then(jsonResponse => {
        if (!("hasDouble" in jsonResponse)) {
            for (let key in jsonResponse) {
                let field = document.querySelector(`.field-${key}`)
                field.querySelector('input').setAttribute('aria-invalid', 'true')
                field.classList.add('text-danger')
                field.querySelector('.help-block').innerHTML = jsonResponse[key]
            }
        }
        if (!jsonResponse.hasDouble) {
            this.parentNode.remove()
            document.querySelector('#sendBtn').removeAttribute('disabled')
        }
    }).catch(error => {
        debugger
        let alertDangerTemplate = document.querySelector('#alert-danger-template')
        let newAlertDanger = alertDangerTemplate.cloneNode(false)

        newAlertDanger.innerHTML = error
        newAlertDanger.classList.remove('d-none')
        alertDangerTemplate.parentElement.append(newAlertDanger)
    })
})