<?php


namespace app\components;


use app\models\Application;
use app\models\ApplicationForm;
use app\models\ApplicationStatus;
use DateTimeImmutable;
use GuzzleHttp\Exception\GuzzleException;
use yii\web\HttpException;

class ApplicationService
{
    public function __construct(
        protected AlfaApi $alfaApi
    ) {
    }

    /**
     * Создаёт модель заявки по объекту формы
     *
     * @param ApplicationForm $form
     * @return Application
     */
    public function getNewApplicationByForm(ApplicationForm $form): Application
    {
        $application = new Application();
        $application->setOrganisationName($form->organisationName);
        $application->setInn($form->inn);
        $application->setOgrn($form->ogrn);
        $application->setKpp($form->kpp);
        $application->setFullName($form->fullName);
        $application->setPhoneNumber($form->phoneNumber);
        $application->setContactEmail($form->contactEmail);
        $application->setContractorsPosition($form->contractorsPosition);
        $application->setExtraPhoneNumbers($form->extraPhoneNumbers);
        $application->setComment($form->comment);
        $application->setCityCode($form->cityCode);
        $application->setProductCode($form->productCode);
        $application->setBranchId($form->branchId);
        $application->setAddress($form->address);
        $application->setEmail($form->email);
        $application->setCreatedAt(new DateTimeImmutable());
        $application->setStatus(ApplicationStatus::makeInWork());

        return $application;
    }

    /**
     * Проверяет на дубли
     *
     * @param string $inn
     * @param array $productCodes
     * @param array $phoneNumbers
     * @return bool
     * @throws GuzzleException
     */
    public function hasDoubles(string $inn, array $productCodes, array $phoneNumbers): bool
    {
        $application = Application::find()->where([
            'inn' => $inn
        ])->one();

        $hasDoubles = $this->alfaApi->hasDoubles(
            inn: $inn,
            productCodes: $productCodes,
            phoneNumbers: $phoneNumbers
        );

        if (!$hasDoubles || null == $application) {
            return false;
        }

        return true;
    }

    /**
     * @param Application $application
     * @return string
     * @throws HttpException
     */
    public function sendApplicationAndGetAlfaId(Application $application): string
    {
        return $this->alfaApi->sendApplicationAndGetAlfaId($application);
    }

    /**
     * @param Application $app
     * @return bool
     * @throws GuzzleException
     * @throws HttpException
     */
    public function updateAppsStatus(Application $app): bool
    {
        $alfaId = $app->getAlfaId();
        if (null === $alfaId) {
            throw new HttpException(
                500,
                "Невозможно обновить статус по заявке {$app->getId()}. У заявки отсутствует alfa_id"
            );
        }

        $newStatus = $this->alfaApi->getActualApplicationStatusByAlfaId($app->getAlfaId());
        $app->setStatus($newStatus);
        $app->setLastCheckStatusAt(new DateTimeImmutable());

        return $app->save(false);
    }
}