<?php


namespace app\components;

use app\models\Application;
use app\models\ApplicationStatus;
use Codeception\Util\HttpCode;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use yii\web\HttpException;

class AlfaApi
{
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://partner.alfabank.ru/public-api/v2/',
            'headers' => [
                'Content-Type' => 'application/json;charset=UTF-8',
                'API-key' => 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa',
                'Accept' => 'application/json'
            ],
        ]);
    }

    /**
     * Проверяет дубли по АПИ Альфа банка
     *
     * @param string $inn
     * @param array $productCodes
     * @param array $phoneNumbers
     * @return bool
     * @throws HttpException
     */
    public function hasDoubles(
        string $inn,
        array $productCodes,
        array $phoneNumbers
    ): bool {
        $productCodesResult = [];
        foreach ($productCodes as $productCode) {
            $productCodesResult[] = ['productCode' => $productCode];
        }

        $phoneNumbersResult = [];
        foreach ($phoneNumbers as $phoneNumber) {
            $phoneNumbersResult[] = ['phoneNumber' => $phoneNumber];
        }

        $data = [
            'organizationInfo' => ['inn' => $inn],
            'productInfo' => $productCodesResult,
        ];

        if (!empty($phoneNumbers)) {
            $data['contactInfo'] = $phoneNumbersResult;
        }

        try {
            $response = $this->client->post('checks', [
                'json' => $data
            ]);
        } catch (\Throwable $e) {
            throw new HttpException(500, 'Ошибка на сервере Альфа Банка');
        }

        if (HttpCode::OK === $response->getStatusCode()) {
            return false;
        }

        return true;
    }

    public function sendApplicationAndGetAlfaId(Application $application): string
    {
        $response = $this->client->post('leads', [
            'json' => [
                'organizationInfo' => [
                    'organizationName' => $application->getOrganisationName(),
                    'inn' => $application->getInn(),
                    'ogrn' => $application->getOgrn(),
                    'kpp' => $application->getKpp(),
                ],
                'contactInfo' => [
                    [
                        'fullName' => $application->getFullName(),
                        'phoneNumber' => $application->getPhoneNumber(),
                        'contactEmail' => $application->getEmail(),
                        'extraPhoneNumbers' => [$application->getExtraPhoneNumbers()],
                    ],
                ],
                'requestInfo' => [
                    'comment' => $application->getComment(),
                    'cityCode' => $application->getCityCode(),
                ],
                'productInfo' => [
                    ['productCode' => $application->getProductCode()],
                ],
                'reserveAccount' => [
                    'branchId' => $application->getBranchId(),
                    'address' => $application->getAddress(),
                    'email' => $application->getContactEmail(),
                ],
            ]
        ]);

        if (HttpCode::OK !== $response->getStatusCode()) {
            throw new HttpException($response->getStatusCode(), $response->getBody()->getContents());
        }

        $responseJson = json_decode($response->getBody()->getContents(), true);
        if (!isset($responseJson['id'])) {
            throw new HttpException(500, 'Некорректный ответ от Альфа Банка. Отсутсвует ключ id');
        }

        return $responseJson['id'];
    }

    /**
     * Делает запрос на АПИ АльфаБанка и возвращает актуальный статус по заявке
     *
     * @param string $alfaId
     * @return ApplicationStatus
     * @throws GuzzleException
     */
    public function getActualApplicationStatusByAlfaId(string $alfaId): ApplicationStatus
    {
        $response = $this->client->get("leads/$alfaId");
        $responseJson = json_decode($response->getBody()->getContents(), true);

        $statusCode = match($response['state']) {
            'DECLINED' => 3,
            'IN_PROGRESS' => 1,
            'CLOSED' => 4,
        };

        return new ApplicationStatus($statusCode);
    }
}