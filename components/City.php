<?php


namespace app\components;


class City
{
    public const MOSCOW = '0c5b2444-70a0-4932-980c-b4dc0d3f02b5';
    public const ST_PETERSBURG   = 'c2deb16a-0330-4f05-821f-1d09c93331e6';
    public const MOSCOW_REG = '29251dcf-00a1-4e34-98d4-5c47484a36d4';
    public const KURSK_REG = 'ee594d5e-30a9-40dc-b9f2-0add1be44ba1';
    public const KAMCHATKA_REG = 'd02f30fc-83bf-4c0f-ac2b-5729a866a207';
    public const KRASNODAR_REG = 'd00e1013-16bd-4c09-b3d5-3cb09fc54bd8';
    public const JEWISH_REG = '1b507b09-48c9-434f-bf6f-65066211c73e';
    // и тд ...

    private string $fias;

    public function __construct(string $fias)
    {
        if (!in_array($fias, array_keys(self::getAssocList()))) {
            throw new \OutOfBoundsException("Невозможно создать объект города с ФИАС = $fias");
        }

        $this->fias = $fias;
    }

    /**
     * @return string[]
     */
    public static function getAssocList()
    {
        return [
            self::MOSCOW => 'Москва г',
            self::ST_PETERSBURG => 'Санкт-Петербург г',
            self::MOSCOW_REG => 'Московская обл',
            self::KURSK_REG => 'Курская обл',
            self::KAMCHATKA_REG => 'Камчатский край',
            self::KRASNODAR_REG => 'Краснодарский край',
            self::JEWISH_REG => 'Еврейская Аобл',
        ];
    }

    public function getFias(): string
    {
        return $this->fias;
    }

    public function getTitle(): string
    {
        return self::getAssocList()[$this->getFias()];
    }
}