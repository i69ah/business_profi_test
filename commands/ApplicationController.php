<?php


namespace app\commands;


use app\components\AlfaApi;
use app\components\ApplicationService;
use app\models\Application;
use app\models\ApplicationStatus;
use GuzzleHttp\Exception\GuzzleException;
use yii\console\Controller;
use yii\web\HttpException;

/**
 * Class ApplicationController
 * @package app\commands
 */
class ApplicationController extends Controller
{
    // cron: @hourly /srv/businessprofi/yii application
    /**
     * Команда обновления статусов по заявкам
     *
     * @throws HttpException
     * @throws GuzzleException
     */
    public function actionIndex()
    {
        $apps = Application::find()->where([
            'status' => ApplicationStatus::makeInWork()->getCode(),
            ['not', ['alfa_id' => null]]
        ])->all();

        $applicationService = new ApplicationService(new AlfaApi());

        foreach ($apps as $app) {
            if (!$applicationService->updateAppsStatus($app)) {
                throw new HttpException(
                    500,
                    "Невозможно обновить статус по заявке {$app->getId()}. Ошибка при сохранении в базу"
                );
            }

            sleep(1.5);
        }

        echo "\nСтатусы по заявкам обновлены\n";
    }
}