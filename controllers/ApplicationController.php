<?php


namespace app\controllers;

use app\components\AlfaApi;
use app\components\ApplicationService;
use app\models\ApplicationForm;
use app\models\ApplicationSearch;
use DateTimeImmutable;
use GuzzleHttp\Exception\GuzzleException;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Контроллер для работы с заявками
 */
class ApplicationController extends Controller
{
    /**
     * Отображение всех заявок + фильтрация по таблице с заявками
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ApplicationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        ]);
    }

    /**
     * Создание новой заявки
     *
     * @return string
     * @throws HttpException
     */
    public function actionNew()
    {
        $form = new ApplicationForm();
        if (!Yii::$app->request->isPost) {
            $title = 'Добавить заявку';

            return $this->render('newApplication', [
                'model' => $form,
                'title' => $title,
            ]);
        }

        $form->attributes = Yii::$app->request->post('ApplicationForm');

        if (!$form->validate()) {
            echo json_encode($form->getErrors());
            exit;
        }

        $applicationService = new ApplicationService(new AlfaApi());
        $app = $applicationService->getNewApplicationByForm($form);

        if (!$app->save()) {
            throw new HttpException(
                500,
                "Невозможно сохранить в базу заявку {$app->getId()}. Ошибка при сохранении в базу"
            );
        }

        $app->setAlfaId($applicationService->sendApplicationByForm($app));
        $app->setLastCheckStatusAt(new DateTimeImmutable());
        if (!$app->save()) {
            throw new HttpException(
                500,
                "Невозможно обновить last_check_status_at у заявки {$app->getId()}. Ошибка при сохранении в базу"
            );
        }

        echo 'Success validation';
    }

    /**
     * Проверка заявки на дубли
     *
     * @return array|false[]
     * @throws HttpException
     * @throws GuzzleException
     */
    public function actionAjaxCheckDouble()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            $form = new ApplicationForm();
            $form->attributes = Yii::$app->request->post('ApplicationForm');
            if (!$form->validate()) {
                return ActiveForm::validate($form);
            }

            $applicationService = new ApplicationService(new AlfaApi());
            $hasDouble = $applicationService->hasDoubles(
                inn: $form->inn,
                productCodes: [$form->productCode],
                phoneNumbers: [$form->phoneNumber],
            );

            return [
                'hasDouble' => $hasDouble
            ];
        }

        throw new HttpException(405);
    }
}
