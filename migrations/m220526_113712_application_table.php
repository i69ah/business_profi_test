<?php

use yii\db\Migration;

/**
 * Class m220526_113712_application_table
 */
class m220526_113712_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('application', [
            'id' => $this->primaryKey()->comment('	ID'),
            'organisation_name' => $this->string(255)->notNull()->comment('Наименование ЮЛ\ИП'),
            'inn' => $this->string(12)->notNull()->comment('ИНН'),
            'ogrn' => $this->string(15)->notNull()->comment('ОГРН или ОГРНИП'),
            'kpp' => $this->string(9)->notNull()->comment('КПП для ООО'),
            'full_name' => $this->string(255)->notNull()->comment('ФИО одной строкой'),
            'phone_number' => $this->string(11)->notNull()->comment('Номер телефона основной'),
            'contact_email' => $this->string(255)->notNull()->comment('Email'),
            'contractors_position' => $this->string(255)->notNull()->comment('Должность'),
            'extra_phone_numbers' => $this->text()->null()->defaultValue(null)->comment('Доп. номера телефонов'),
            'comment' => $this->text()->notNull()->comment('Комментарий к заявке'),
            'city_code' => $this->string(255)->notNull()->comment('Код города (ФИАС)'),
            'product_code' => $this->string(255)->notNull()->comment('Код продукта'),
            'branch_id' => $this->integer()->notNull()->comment('Код отделения'),
            'address' => $this->text()->notNull()->comment('Адрес организации'),
            'email' => $this->string(255)->notNull()->comment('Email на который уйдет письмо о резерве'),
            'created_at' => $this->dateTime()->notNull()->comment('Дата создания записи'),
            'last_check_status_at' => $this->dateTime()->null()->defaultValue(null)->comment('Дата последней проверки статуса заявки'),
            'status' => $this->integer()->notNull()->defaultValue(1)->comment('	Статус заявки (1 - в работе; 2 - дубль; 3 - отклонён; 4 - успешно)'),
            'alfa_if' => $this->string(255)->null()->defaultValue(null)->comment('ID заявки в Альфа Банке')
        ]);

        $this->insert('application', [
            'organisation_name' => "McDonald's",
            'inn' => '1234567890',
            'ogrn' => '12345678910000',
            'kpp' => '123456789',
            'full_name' => "ООО McDonald's",
            'phone_number' => '79998887766',
            'contact_email' => 'mcdonalds@mcd.com',
            'contractors_position' => 'Директор',
            'extra_phone_numbers' => '79998887765 79998887767',
            'comment' => '',
            'city_code' => '0c5b2444-70a0-4932-980c-b4dc0d3f02b5',
            'product_code' => 'LP_RKO',
            'branch_id' => 1,
            'address' => 'Россия, г. Москва, Малая Бронная, д.3',
            'email' => 'mcdonalds@mcd.com',
            'created_at' => '2022-05-24 15:33:32',
            'last_check_status_at' => null,
            'status' => 1,
            'alfa_if' => null,
        ]);

        $this->insert('application', [
            'organisation_name' => "Valve",
            'inn' => '0987654321',
            'ogrn' => '12345678910001',
            'kpp' => '123456790',
            'full_name' => "Valve Inn",
            'phone_number' => '79135678990',
            'contact_email' => 'valveinn@vlv.com',
            'contractors_position' => 'Директор',
            'extra_phone_numbers' => '',
            'comment' => '',
            'city_code' => 'c2deb16a-0330-4f05-821f-1d09c93331e6',
            'product_code' => 'LP_RKO',
            'branch_id' => 1,
            'address' => 'Россия, г. Санкт-Петербург, Невский проспект, д.15',
            'email' => 'valveinn@vlv.com',
            'created_at' => '2022-05-25 10:17:05',
            'last_check_status_at' => null,
            'status' => 1,
            'alfa_if' => null,
        ]);

        $this->insert('application', [
            'organisation_name' => "Salvador",
            'inn' => '0987612345',
            'ogrn' => '12345678910002',
            'kpp' => '123456791',
            'full_name' => "Salvador Corporation",
            'phone_number' => '79918556211',
            'contact_email' => 'salvador@salvcorp.com',
            'contractors_position' => 'CEO',
            'extra_phone_numbers' => '',
            'comment' => 'Просим скорейшего рассмотрения',
            'city_code' => 'c2deb16a-0330-4f05-821f-1d09c93331e6',
            'product_code' => 'LP_RKO',
            'branch_id' => 1,
            'address' => 'Россия, г. Санкт-Петербург, Невский проспект, д.14',
            'email' => 'salvador@salvcorp.com',
            'created_at' => '2022-05-26 12:23:56',
            'last_check_status_at' => null,
            'status' => 1,
            'alfa_if' => null,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('application', ['id' => 3]);
        $this->delete('application', ['id' => 2]);
        $this->delete('application', ['id' => 1]);
        $this->dropTable('application');
    }

}
