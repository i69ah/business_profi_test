<?php


namespace app\models;


use DateTimeImmutable;
use DateTimeInterface;
use yii\db\ActiveRecord;

/**
 * Class Application
 * @package app\models
 * @property int $id
 * @property string $organisation_name;
 * @property string $inn
 * @property string $ogrn
 * @property string $kpp
 * @property string $full_name
 * @property string $phone_number
 * @property string $contact_email
 * @property string $contractors_position
 * @property string|null $extra_phone_numbers
 * @property string $comment
 * @property string $city_code
 * @property string $product_code
 * @property string $branch_id
 * @property string $address
 * @property string $email
 * @property string $created_at
 * @property string|null $last_check_status_at
 * @property int $status
 * @property string|null $alfa_id
 */
class Application extends ActiveRecord
{
    public static function tableName()
    {
        return '{{application}}';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOrganisationName(): string
    {
        return $this->organisation_name;
    }

    /**
     * @param string $organisationName
     */
    public function setOrganisationName(string $organisationName): void
    {
        $this->organisation_name = $organisationName;
    }

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn): void
    {
        $this->inn = $inn;
    }

    /**
     * @return string
     */
    public function getOgrn(): string
    {
        return $this->ogrn;
    }

    /**
     * @param string $ogrn
     */
    public function setOgrn(string $ogrn): void
    {
        $this->ogrn = $ogrn;
    }

    /**
     * @return string
     */
    public function getKpp(): string
    {
        return $this->kpp;
    }

    /**
     * @param string $kpp
     */
    public function setKpp(string $kpp): void
    {
        $this->kpp = $kpp;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->full_name;
    }

    /**
     * @param string $fullName
     */
    public function setFullName(string $fullName): void
    {
        $this->full_name = $fullName;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phone_number;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phone_number = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getContactEmail(): string
    {
        return $this->contact_email;
    }

    /**
     * @param string $contactEmail
     */
    public function setContactEmail(string $contactEmail): void
    {
        $this->contact_email = $contactEmail;
    }

    /**
     * @return string
     */
    public function getContractorsPosition(): string
    {
        return $this->contractors_position;
    }

    /**
     * @param string $contractorsPosition
     */
    public function setContractorsPosition(string $contractorsPosition): void
    {
        $this->contractors_position = $contractorsPosition;
    }

    /**
     * @return string|null
     */
    public function getExtraPhoneNumbers(): ?string
    {
        return $this->extra_phone_numbers;
    }

    /**
     * @param string|null $extraPhoneNumbers
     */
    public function setExtraPhoneNumbers(?string $extraPhoneNumbers): void
    {
        $this->extra_phone_numbers = $extraPhoneNumbers;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getCityCode(): string
    {
        return $this->city_code;
    }

    /**
     * @param string $cityCode
     */
    public function setCityCode(string $cityCode): void
    {
        $this->city_code = $cityCode;
    }

    /**
     * @return string
     */
    public function getProductCode(): string
    {
        return $this->product_code;
    }

    /**
     * @param string $productCode
     */
    public function setProductCode(string $productCode): void
    {
        $this->product_code = $productCode;
    }

    /**
     * @return string
     */
    public function getBranchId(): string
    {
        return $this->branch_id;
    }

    /**
     * @param string $branchId
     */
    public function setBranchId(string $branchId): void
    {
        $this->branch_id = $branchId;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return DateTimeImmutable::createFromFormat($this->created_at, 'Y-m-d H:i:s');
    }

    /**
     * @param DateTimeInterface $createdAt
     */
    public function setCreatedAt(DateTimeInterface $createdAt): void
    {
        $this->created_at = $createdAt->format('Y-m-d H:i:s');
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastCheckStatusAt(): ?DateTimeInterface
    {
        return match($this->last_check_status_at) {
            null => null,
            default => DateTimeImmutable::createFromFormat($this->last_check_status_at, 'Y-m-d H:i:s')
        };
    }

    /**
     * @param DateTimeInterface $lastCheckStatusAt
     */
    public function setLastCheckStatusAt(DateTimeInterface $lastCheckStatusAt): void
    {
        $this->last_check_status_at = $lastCheckStatusAt->format('Y-m-d H:i:s');
    }

    /**
     * @return ApplicationStatus
     */
    public function getStatus(): ApplicationStatus
    {
        return new ApplicationStatus($this->status);
    }

    /**
     * @param ApplicationStatus $status
     */
    public function setStatus(ApplicationStatus $status): void
    {
        $this->status = $status->getCode();
    }

    /**
     * @return string|null
     */
    public function getAlfaId(): ?string
    {
        return $this->alfa_id;
    }

    /**
     * @param string|null $alfaId
     */
    public function setAlfaId(?string $alfaId): void
    {
        $this->alfa_id = $alfaId;
    }
}
