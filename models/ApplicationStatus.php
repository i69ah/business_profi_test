<?php


namespace app\models;

use OutOfBoundsException;

/**
 * Class ApplicationStatus
 * @package app\models
 */
class ApplicationStatus
{
    public const IN_WORK = 1;

    public const DOUBLE = 2;

    public const REJECTED = 3;

    public const SUCCESS = 4;

    protected int $code;

    /**
     * ApplicationStatus constructor.
     * @param int $code
     */
    public function __construct(int $code)
    {
        if (!in_array($code, array_keys(self::getAssocList()))) {
            throw new OutOfBoundsException("Невозможно создать статус заявки с кодом $code");
        }

        $this->code = $code;
    }

    /**
     * @return string[]
     */
    public static function getAssocList(): array
    {
        return [
            self::IN_WORK => 'В работе',
            self::DOUBLE => 'Дубль',
            self::REJECTED => 'Отклонена',
            self::SUCCESS => 'Успешно',
        ];
    }

    /**
     * @return static
     */
    public static function makeInWork(): self
    {
        return new self(self::IN_WORK);
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return self::getAssocList()[$this->getCode()];
    }

    /**
     * @return bool
     */
    public function isInWork(): bool
    {
        return self::IN_WORK === $this->getCode();
    }

    /**
     * @return bool
     */
    public function isDouble(): bool
    {
        return self::DOUBLE === $this->getCode();
    }

    /**
     * @return bool
     */
    public function isRejected(): bool
    {
        return self::REJECTED === $this->getCode();
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return self::SUCCESS === $this->getCode();
    }
}