<?php


namespace app\models;


use yii\data\ActiveDataProvider;

class ApplicationSearch extends Application
{
    public $created_at_from;
    public $created_at_to;
    public $last_check_status_at_from;
    public $last_check_status_at_to;

    public function rules()
    {
        return [
            [['created_at_from', 'created_at_to'], 'datetime', 'format' => 'Y-m-d'],
            [['last_check_status_at_from', 'last_check_status_at_to'], 'datetime', 'format' => 'Y-m-d'],
            [['status'], 'in', 'range' => array_keys(ApplicationStatus::getAssocList())]
        ];
    }

    public function attributeLabels()
    {
        return [
            'created_at_from' => 'Дата создания (от)',
            'created_at_to' => 'Дата создания (до)',
            'last_check_status_at_from' => 'Дата обновления статуса (от)',
            'last_check_status_at_to' => 'Дата обновления статуса (до)',
            'status' => 'Статус заявки'
        ];
    }

    public function search($params)
    {
        $this->load($params);
        $query = Application::find();

        if (!empty($this->created_at_from)) {
            $query->andFilterWhere(['>=', 'created_at', $this->created_at_from]);
        }

        if (!empty($this->created_at_to)) {
            $query->andFilterWhere(['<=', 'created_at', $this->created_at_to]);
        }

        if (!empty($this->status)) {
            $query->andFilterWhere(['status' => $this->status]);
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}