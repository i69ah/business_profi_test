<?php


namespace app\models;


use yii\base\Model;

class ApplicationForm extends Model
{
    public $organisationName;
    public $inn;
    public $ogrn;
    public $kpp;

    public $fullName;
    public $phoneNumber;
    public $contactEmail;
    public $contractorsPosition;
    public $extraPhoneNumbers;

    public $comment;
    public $cityCode;

    public $productCode;

    public $branchId;
    public $address;
    public $email;

    public function rules()
    {
        return [
            [['organisationName', 'inn', 'ogrn', 'kpp', 'phoneNumber',
                'cityCode', 'productCode', 'branchId', 'address', 'email'], 'required'],
            ['inn', 'match', 'pattern' => '/^(\d{10})$|^(\d{12})$|^$/'],
            ['ogrn', 'match', 'pattern' => '/^(\d{13})$|^(\d{15})$|^$/'],
            ['kpp', 'match', 'pattern' => '/\d{9}/'],
            ['phoneNumber', 'match', 'pattern' => '/^((\+7|7|8)+([0-9]){10})$/'],
            ['extraPhoneNumbers', 'match', 'pattern' => '/^((\+7|7|8)+([0-9]){10})$/'],
            ['email', 'email'],
            [['fullName', 'contactEmail', 'contractorsPosition', 'extraPhoneNumbers', 'comment'], 'safe'],
            [['phoneNumber', 'extraPhoneNumbers'], 'filter', 'filter' => [$this, 'normalizePhone']]
        ];
    }

    public function attributeLabels()
    {
        return [
            'organisationName' => 'Наименование ЮЛ\ИП',
            'inn' => 'ИНН',
            'ogrn' => 'ОГРН или ОГРНИП',
            'kpp' => 'КПП для ООО',
            'fullName' => 'ФИО одной строкой',
            'phoneNumber' => 'Номер телефона основной',
            'contactEmail' => 'Email',
            'contractorsPosition' => 'Должность',
            'extraPhoneNumbers' => 'Доп. номера телефонов',
            'сomment' => 'Комментарий к заявке',
            'cityCode' => 'Код города (ФИАС)',
            'productCode' => 'Код продукта',
            'branchId' => 'Код отделения',
            'address' => 'Адрес организаци',
            'email' => 'Email, на который уйдёт письмо о резерве',
        ];
    }

    public function normalizePhone($value)
    {
        $newValue = $value;
        if (str_starts_with($value, '+')) {
            $newValue = substr($value,1);
        }

        if (str_starts_with($value, '8')) {
            $newValue = '7' . substr($value,1);
        }

        return $newValue;
    }
}